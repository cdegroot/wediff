import os

# Get the required environment variables
env = Environment(ENV = os.environ)

# Clone the environment to set up the required c++ environment
cppEnv = Environment().Clone()

# Set the compiler, if needed
try:
    cppEnv['CXX'] = env['ENV']['CXX']
    if cppEnv['CXX'] == 'clang++':
        cppEnv.Append(CXX_FLAGS = ['-stdlib=libc++'])
        cppEnv.Append(LINKFLAGS = ['-stdlib=libc++'])
except KeyError:
    pass

# Allow undefined symbols on Mac
if Platform().name == 'darwin':
    cppEnv.Append(LINKFLAGS = ['-undefined', 'dynamic_lookup'])

# Set compiler flags
cppEnv.Append(CXXFLAGS = ['-Wall', '-O3', '-std=c++11'])

# Set path to source files
cppEnv.Append(CPPPATH = [Dir('#src').abspath])

# Set up profiling, if needed
try:
    if env['ENV']['WEDIFF_PROFILING'] == 'true':
        cppEnv.Append(LIBS = ['profiler'])
except KeyError:
    pass

# Clone the c++ environment to set up the SWIG environment
swigEnv = cppEnv.Clone()

# Set up boost paths in c++ environment
try:
    cppEnv.Append(CPPPATH = [env['ENV']['BOOST_INCLUDE_DIR']])
except KeyError:
    cppEnv.Append(CPPPATH = '/usr/local/include/boost')
try:
    cppEnv.Append(LIBPATH = [env['ENV']['BOOST_LIB_DIR']])
except KeyError:
    cppEnv.Append(LIBPATH = '/usr/local/lib')

# Set up Python include directory
swigEnv.Append(CPPPATH = [env['ENV']['PYTHON_INCLUDE_DIR']])

# Set up Python library directory
try:
    pythonLib = env['ENV']['PYTHON_LIB_DIR']
except KeyError:
    pythonLib = '/usr/local/lib'
swigEnv.Append(LIBPATH = pythonLib)

# Set up Python version
try:
    pythonVersion = env['ENV']['PYTHON_VERSION']
except KeyError:
    pythonVersion = ''

# Set up Python site-packages location
pythonSitePkg = env['ENV']['PYTHON_SITE_PACKAGES']

# Set up include directories for SWIG
for path in swigEnv['CPPPATH']:
	swigEnv.Append(SWIGFLAGS = ['-I%s' % path])

# Set SWIG-specific flags
if Platform().name == 'darwin':
    pass
else:
    swigEnv.Append(SHLINKFLAGS = ['-lpython%s' % pythonVersion, '-shared'])
swigEnv.Append(SWIGFLAGS = ['-python', '-c++', '-Wall'])
swigEnv['SHLIBPREFIX'] = '_'
swigEnv['SHLIBSUFFIX'] = '.so'

# Get the install directory
try:
    install_dir = env['ENV']['WEDIFF_INST_DIR']
except KeyError:
    install_dir = '/usr/local/'

# Set up aliases to install the library
cppEnv.Alias('install', install_dir)
swigEnv.Alias('install', install_dir)

# Export the environments and install directory variable
Export('cppEnv')
Export('swigEnv')
Export('install_dir')
Export('pythonSitePkg')

# Call the SConscript for the c++ sources
cppEnv.SConscript('src/SConscript', variant_dir='build', duplicate=0)
