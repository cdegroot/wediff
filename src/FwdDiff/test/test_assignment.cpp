/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(Assignment)

/**
 * Test simple assignment for FwdDiff<float> from float and double
 */
BOOST_AUTO_TEST_CASE(testFloatSimpleAssignment)
{
  float f = 1.0;
  double d = 2.0;
  FwdDiff<float> a;

  a = f;

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);

  a = d;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);
}

/**
 * Test simple assignment for FwdDiff<double> from float and double
 */
BOOST_AUTO_TEST_CASE(testDoubleSimpleAssignment)
{
  float f = 1.0;
  double d = 2.0;
  FwdDiff<double> a;

  a = f;

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);

  a = d;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);
}

/**
 * Test simple assignment for FwdDiff types
 */
BOOST_AUTO_TEST_CASE(testFwdDiffSimpleAssignment)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);
  a = b;

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
  BOOST_CHECK_CLOSE(b.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), 2.0, SMALL);
}

/**
 * Test addition assignment with a constant
 */
BOOST_AUTO_TEST_CASE(testConstantAdditionAssignment)
{
  FwdDiff<double> a;

  a.setVal(1.0);
  a.setDx(2.0);
  a += 1.0;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
}

/**
 * Test addition assignment for FwdDiff types
 */
BOOST_AUTO_TEST_CASE(testFwdDiffAdditionAssignment)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  a += b;

  BOOST_CHECK_CLOSE(a.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 6.0, SMALL);
}

/**
 * Test subtraction assignment with a constant
 */
BOOST_AUTO_TEST_CASE(testConstantSubtractionAssignment)
{
  FwdDiff<double> a;

  a.setVal(1.0);
  a.setDx(2.0);
  a -= 1.0;

  BOOST_CHECK_CLOSE(a.val(), 0.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
}

/**
 * Test subtraction assignment for FwdDiff types
 */
BOOST_AUTO_TEST_CASE(testFwdDiffSubtractionAssignment)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  a -= b;

  BOOST_CHECK_CLOSE(a.val(), -2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -2.0, SMALL);
}

/**
 * Test multiplication assignment with a constant
 */
BOOST_AUTO_TEST_CASE(testConstantMultiplicationAssignment)
{
  FwdDiff<double> a;

  a.setVal(1.0);
  a.setDx(2.0);

  a *= 2.0;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 4.0, SMALL);
}

/**
 * Test multiplication assignment for FwdDiff types
 */
BOOST_AUTO_TEST_CASE(testFwdDiffMultiplicationAssignment)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  a *= b;

  BOOST_CHECK_CLOSE(a.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 10.0, SMALL);
}

/**
 * Test division assignment with a constant
 */
BOOST_AUTO_TEST_CASE(testConstantDivisionAssignment)
{
  FwdDiff<double> a;

  a.setVal(1.0);
  a.setDx(2.0);

  a /= 2.0;

  BOOST_CHECK_CLOSE(a.val(), 0.5, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0, SMALL);
}

/**
 * Test division assignment for FwdDiff types
 */
BOOST_AUTO_TEST_CASE(testFwdDiffDivisionAssignment)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  a /= b;

  BOOST_CHECK_CLOSE(a.val(), 1.0/3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0/9.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
