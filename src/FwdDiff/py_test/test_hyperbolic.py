"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff
from wediff.double import math
import math as double_math

class TestHyperbolic(object):
    """Tests hyperbolic trigonometry functions"""
    
    def test_hyperbolic_sine(self):
        """Test hyperbolic sine function"""
        b = FwdDiff(2.0, [1.0])
        a = math.sinh(b)

        assert a.val() == double_math.sinh(2.0)
        assert a.dx(0) == double_math.cosh(2.0)

    def test_hyperbolic_cosine(self):
        """Test hyperbolic cosine function"""
        b = FwdDiff(2.0, [1.0])
        a = math.cosh(b)

        assert a.val() == double_math.cosh(2.0)
        assert a.dx(0) == double_math.sinh(2.0)

    def test_hyperbolic_tangent(self):
        """Test hyperbolic tangent function"""
        b = FwdDiff(2.0, [1.0])
        a = math.tanh(b)

        assert a.val() == double_math.tanh(2.0)
        assert a.dx(0) == 1.0/double_math.cosh(2.0)**2.0
        
    def test_asinh(self):
        """Test asinh function"""
        b = FwdDiff(2.0, [1.0])
        a = math.asinh(b)

        assert a.val() == double_math.asinh(2.0)
        assert a.dx(0) == 1.0/double_math.sqrt(5.0)
        
    def test_acosh(self):
        """Test acosh function"""
        b = FwdDiff(2.0, [1.0])
        a = math.acosh(b)

        assert a.val() == double_math.acosh(2.0)
        assert a.dx(0) == 1.0/double_math.sqrt(3.0)
        
    def test_atanh(self):
        """Test atanh function"""
        b = FwdDiff(0.5, [1.0])
        a = math.atanh(b)

        assert a.val() == double_math.atanh(0.5)
        assert a.dx(0) == 4.0/3.0
