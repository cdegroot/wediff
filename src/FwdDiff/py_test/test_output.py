"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff import FwdDiffManager
from wediff.double import FwdDiff

class TestOutput(object):
    """Tests output functions"""
    
    def test_standard_output(self, capfd):
        """Test standard output"""
        a = FwdDiff(1.0, [2.0])
        print(a)        
        out, err = capfd.readouterr()
                
        assert out == "[1 , 2]\n"
        
        a = FwdDiff(1.1, [2.1])
        print(a)        
        out, err = capfd.readouterr()
                
        assert out == "[1.1 , 2.1]\n"
        
        FwdDiffManager().setNumComponents(2)
        
        assert FwdDiffManager().getNumComponents() == 2
        
        a = FwdDiff(1.2, [2.2, 3.2])
        print(a)        
        out, err = capfd.readouterr()
                
        assert out == "[1.2 , 2.2 , 3.2]\n"

        FwdDiffManager().reset()
        
        assert FwdDiffManager().getNumComponents() == 1