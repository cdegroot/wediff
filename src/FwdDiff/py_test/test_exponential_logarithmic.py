"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff
from wediff.double import math
import math as double_math

class TestExponentialLogarithmic(object):
    """Tests exponential and logarithmic functions"""
    
    def test_exponential(self):
        """Test exponential function"""
        b = FwdDiff(2.0, [1.0])
        a = math.exp(b)

        assert a.val() == double_math.exp(2.0)
        assert a.dx(0) == double_math.exp(2.0)

    def test_exponential_minus_one(self):
        """Test exponential minus one function"""
        b = FwdDiff(2.0, [1.0])
        a = math.expm1(b)

        assert a.val() == double_math.expm1(2.0)
        assert a.dx(0) == double_math.exp(2.0)
        
    def test_log(self):
        """Test natural log function"""
        b = FwdDiff(2.0, [1.0])
        a = math.log(b)

        assert a.val() == double_math.log(2.0)
        assert a.dx(0) == 0.5
        
    def test_log10(self):
        """Test log base 10 function"""
        b = FwdDiff(2.0, [1.0])
        a = math.log10(b)

        assert a.val() == double_math.log10(2.0)
        assert a.dx(0) == double_math.log10(double_math.exp(1.0))/2.0
        
    def test_log_plus_one(self):
        """Test log plus 1 function"""
        b = FwdDiff(2.0, [1.0])
        a = math.log1p(b)

        assert a.val() == double_math.log1p(2.0)
        assert a.dx(0) == 1.0/3.0