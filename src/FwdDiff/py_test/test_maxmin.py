"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestMaxMin(object):
    """Tests max and min functions"""
    
    def test_max(self):
        """Test maximum function"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        
        c = max(a, b)

        assert c.val() == 3.0
        assert c.dx(0) == 4.0

    def test_min(self):
        """Test minimum function"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        
        c = min(a, b)

        assert c.val() == 1.0
        assert c.dx(0) == 2.0