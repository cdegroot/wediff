/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(UnaryOperators)

/**
 * Test unary plus
 */
BOOST_AUTO_TEST_CASE(testUnaryPlus)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = +b;

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
  BOOST_CHECK_CLOSE(b.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), 2.0, SMALL);
}

/**
 * Test unary minus
 */
BOOST_AUTO_TEST_CASE(testUnaryMinus)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = -b;

  BOOST_CHECK_CLOSE(a.val(), -1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -2.0, SMALL);
  BOOST_CHECK_CLOSE(b.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), 2.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
