"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestSetters(object):
    """Tests setter function"""
    
    def test_setters(self):
        """Test setters"""
        a = FwdDiff(1.0, [2.0])
        a.setVal(2.0)
        a.setDx(3.0)
        
        assert a.val() == 2.0
        assert a.dx(0) == 3.0 