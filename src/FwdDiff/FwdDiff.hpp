/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#ifndef _FWD_DIFF_HPP_
#define _FWD_DIFF_HPP_

#include "FwdDiffManager.hpp"

#include <boost/numeric/ublas/vector.hpp>
#include <assert.h>
#include <math.h>

/**
 * @file  FwdDiff.hpp
 * @brief Contains FwdDiff class definition
 */

/**
 * @class FwdDiff
 * @brief Defines a data type that carries out forward mode automatic differentiation.
 *
 */
template <typename T> 
class FwdDiff
{
public:

  /**
   * @typedef Vector
   * @brief   Type defining a mathematical vector
   */

  typedef typename boost::numeric::ublas::vector<T> Vector;

  /**
   *  @name Constructors
   */
  ///@{

  /** Default constructor */
  FwdDiff()
  {
    _val = T(0.0);
    _dx = Vector(FwdDiffManager().getNumComponents());
    for (size_t i = 0; i != _dx.size(); ++i)
    {
      _dx[i] = T(0.0);
    }
  }

  /** Constructor with value specified */
  FwdDiff(const T x)
  {
    _val = x;
    _dx = Vector(FwdDiffManager().getNumComponents());
    for (size_t i = 0; i != _dx.size(); ++i)
    {
      _dx[i] = T(0.0);
    }
  }

  /** Constructor from given value and derivative vector  */
  FwdDiff(const T x, const Vector dx)
  {
    assert(FwdDiffManager().getNumComponents() == dx.size());
    _val = x;
    _dx = dx;
  }

  ///@} // Constructors

  /**
   *  @name Unary Operators
   */
  ///@{

  /** Unary plus */
  FwdDiff operator+() const
  {
    return FwdDiff<T>(*this);
  }

  /** Unary minus */
  FwdDiff operator-() const
  {
    return FwdDiff<T>(*this) *= T(-1);
  }

  ///@} // Unary Operators

  /**
   *  @name Assignment Operators
   */
  ///@{

  /** Simple assignment operator with type T */
  FwdDiff& operator=(const T x)
  {
    _val = x;
    for (size_t i = 0; i != _dx.size(); ++i)
    {
      _dx[i] = T(0.0);
    }
    return *this;
  }

  /** Addition-assignment operator with type T */
  FwdDiff& operator+=(const T x)
  {
    _val += x;
    return *this;
  }

  /** Addition-assignment operator with type FwdDiff
   *
   *  Derivative calculation:
   *
   *   \f{eqnarray*}{
   *           z &=& x + y \\
   *    \delta z &=& \frac{\partial z}{\partial x}\delta x + \frac{\partial z}{\partial y}\delta y \\
   *    \delta z &=& \delta x + \delta y
   *   \f}
   */
  FwdDiff& operator+=(const FwdDiff x)
  {
    _dx += x.dx();
    _val += x.val();
    return *this;
  }

  /** Subtraction-assignment operator with type T */
  FwdDiff& operator-=(const T x)
  {
    _val -= x;
    return *this;
  }

  /** Subtraction-assignment operator with type FwdDiff
   *
   *  Derivative calculation:
   *
   *   \f{eqnarray*}{
   *           z &=& x - y \\
   *    \delta z &=& \frac{\partial z}{\partial x}\delta x + \frac{\partial z}{\partial y}\delta y \\
   *    \delta z &=& \delta x - \delta y
   *   \f}
   */
  FwdDiff& operator-=(const FwdDiff x)
  {
    _dx -= x.dx();
    _val -= x.val();
    return *this;
  }

  /** Multiplication-assignment operator with type T */
  FwdDiff& operator*=(const T x)
  {
    _dx *= x;
    _val *= x;
    return *this;
  }

  /** Multiplication-assignment operator with type FwdDiff
   *
   *  Derivative calculation:
   *
   *   \f{eqnarray*}{
   *           z &=& xy \\
   *    \delta z &=& \frac{\partial z}{\partial x}\delta x + \frac{\partial z}{\partial y}\delta y \\
   *    \delta z &=& y\delta x + x\delta y
   *   \f}
   */
  FwdDiff& operator*=(const FwdDiff x)
  {
    _dx = _dx*x.val() + _val*x.dx();
    _val *= x.val();
    return *this;
  }

  /** Division-assignment operator with type T */
  FwdDiff& operator/=(const T x) {
    _dx /= x;
    _val /= x;
    return *this;
  }

  /** Division-assignment operator with type FwdDiff
   *
   *   Derivative calculation:
   *
   *   \f{eqnarray*}{
   *           z &=& \frac{x}{y} \\
   *    \delta z &=& \frac{\partial z}{\partial x}\delta x + \frac{\partial z}{\partial y}\delta y \\
   *    \delta z &=& \frac{\delta x}{y} - \frac{x \delta y}{y^2}
   *   \f}
   */
  FwdDiff& operator/=(const FwdDiff x)
  {
    _dx = _dx/x.val() - _val*x.dx()/(pow(x.val(), 2.0)); // Do a safe divide?
    _val /= x.val();
    return *this;
  }

  ///@} // Assignment Operators

  /**
   *  @name Access Methods
   */
  ///@{

  /** Set value */
  void setVal(const T x)
  {
    _val = x;
  }

  /** Set derivative */
  void setDx(const T x, size_t i = 0)
  {
    _dx[i] = x;
  }

  /** Get value */
  T val() const
  {
    return _val;
  }

  /** Get derivative */
  T dx(size_t i) const
  {
    return _dx[i];
  }

  /** Get derivative vector */
  Vector dx() const
  {
    return _dx;
  }

  ///@} // Access Methods

private:

  /** Value */
  T _val;

  /** Derivative */
  Vector _dx;

};

#include "FwdDiff_Inline.hpp"

#endif
