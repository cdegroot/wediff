"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2018 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

from wediff.double import FwdDiff
from wediff.double import math

def f(x):
    """Generic mathematical function"""
    return x*math.sin(math.pow(x, 2.0)) + x

# Create a FwdDiff number
x = FwdDiff(2.0, [1.0])

# Evaluate the function
y = f(x)

# Report the derivative
print("df/dx at x = 2.0: {}".format(y.dx(0)))
