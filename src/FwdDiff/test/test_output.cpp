/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

// Use test stream for output
using boost::test_tools::output_test_stream;

BOOST_AUTO_TEST_SUITE(Output)

/**
 * Test standard output
 */
BOOST_AUTO_TEST_CASE(testStandardOutput)
{
  FwdDiff<double> a;

  a.setVal(1.0);
  a.setDx(2.0);
  output_test_stream output;

  output << a;

  BOOST_CHECK(output.is_equal("[1 , 2]"));

  a = 1.1;
  a.setDx(2.1);

  output << a;

  BOOST_CHECK(output.is_equal("[1.1 , 2.1]"));

  FwdDiffManager().setNumComponents(2);
  FwdDiff<double> b;

  b.setVal(1.2);
  b.setDx(2.2, 0);
  b.setDx(3.2, 1);

  output << b;

  BOOST_CHECK(output.is_equal("[1.2 , 2.2 , 3.2]"));
  FwdDiffManager().reset();
}

BOOST_AUTO_TEST_SUITE_END()
