/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(HyperbolicFunctions)

/**
 * Test hyperbolic sine function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicSine)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = sinh(b);

  BOOST_CHECK_CLOSE(a.val(), sinh(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), cosh(2.0), SMALL);
}

/**
 * Test hyperbolic cosine function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicCosine)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = cosh(b);

  BOOST_CHECK_CLOSE(a.val(), cosh(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), sinh(2.0), SMALL);
}

/**
 * Test hyperbolic tangent function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicTangent)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = tanh(b);

  BOOST_CHECK_CLOSE(a.val(), tanh(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/pow(cosh(2.0), 2.0), SMALL);
}

/**
 * Test hyperbolic arcsin function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicArcsin)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = asinh(b);

  BOOST_CHECK_CLOSE(a.val(), asinh(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/sqrt(5.0), SMALL);
}

/**
 * Test hyperbolic arccos function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicArccos)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = acosh(b);

  BOOST_CHECK_CLOSE(a.val(), acosh(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/sqrt(3.0), SMALL);
}

/**
 * Test hyperbolic arctan function
 */
BOOST_AUTO_TEST_CASE(testHyperbolicArctan)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(0.5);
  b.setDx(1.0);

  a = atanh(b);

  BOOST_CHECK_CLOSE(a.val(), atanh(0.5), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 4.0/3.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
