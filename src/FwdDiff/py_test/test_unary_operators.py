"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestUnaryOperators(object):
    """Tests unary operators"""
    
    def test_unary_plus(self):
        """Test unary plus operator"""
        a = FwdDiff(1.0, [2.0])
        b = +a
                        
        assert b.val() == a.val()
        assert b.dx(0) == a.dx(0)
        
    def test_unary_minus(self):
        """Test unary minus operator"""
        a = FwdDiff(1.0, [2.0])
        b = -a
                        
        assert b.val() == -a.val()
        assert b.dx(0) == -a.dx(0)